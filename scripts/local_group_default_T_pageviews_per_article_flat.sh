#!/bin/bash

#Pageviews per article query
domain="analytics.wikimedia.org"
project="en.wikipedia"
articles="('Banana', 'Earth', 'Moon', 'Wikipedia', 'Museo_del_Prado')"
granularities="('daily')"     # monthly granularity will be created programatically
start_timestamp="20180101"
end_timestamp="20211231"
output_filename="local_group_default_T_pageviews_per_article_flat.csv"

printf "SELECT * FROM \"local_group_default_T_pageviews_per_article_flat\".data\
 WHERE \"_domain\" = '$domain' AND project = '$project' AND article IN $articles
 AND granularity IN $granularities AND timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename