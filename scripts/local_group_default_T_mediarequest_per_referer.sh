#!/bin/bash

#mediarequest per referer query
domain="analytics.wikimedia.org"
referers="('all-referers', 'internal', 'external', 'search-engine', 'unknown', 'none')"
media_types="('all-media-types', 'image', 'audio', 'video', 'document', 'other')"
agents="('all-agents', 'spider', 'user')"
granularities="('daily', 'monthly')"
start_timestamp="20180101"
end_timestamp="21211231"
output_filename="local_group_default_T_mediarequest_per_referer.csv"

printf "SELECT * FROM \"local_group_default_T_mediarequest_per_referer\".data\
 WHERE \"_domain\" = '$domain' AND referer IN $referers and media_type IN $media_types AND agent IN $agents AND granularity IN $granularities AND\
 timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename