#!/bin/bash

#Top by country query
domain="analytics.wikimedia.org"
project="en.wikipedia"
access_sites="('all-access', 'desktop', 'mobile-app', 'mobile-web')"
year_range="('2019', '2020')"
month_range=()
day_range=()
output_filename="local_group_default_T_top_pageviews.csv"

#fill month range
for i in 0{1..9} {10..12}; do
    month_range+=(\'${i}\')
done
IFS=", "
month_range="(${month_range[*]})"

# fill day range
for i in 0{1..9} {10..31} "all-days"; do
    day_range+=(\'${i}\')
done
IFS=", "
day_range="(${day_range[*]})"

printf "SELECT * FROM \"local_group_default_T_top_pageviews\".data
 WHERE \"_domain\" = '$domain' AND project = '$project' AND 
 access IN $access_sites AND year IN $year_range AND month IN $month_range AND day IN $day_range;" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename