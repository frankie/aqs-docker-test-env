#!/bin/bash

# mediarequest per file query
domain="analytics.wikimedia.org"
referers="('all-referers', 'internal', 'external', 'search-engine', 'unknown', 'none')"
# %% -> escaped percent sign
file_paths="('/wikipedia/commons/1/1c/Manhattan_Bridge_Construction_1909.jpg', 
 '/wikipedia/commons/6/60/The_Earth_4K_Extended_Edition.webm', 
 '/wikipedia/commons/b/bd/Titan_%%28moon%%29.ogg',
 '/wikipedia/commons/7/7e/NPS_craters-of-the-moon-map.pdf')"
granularities="('daily', 'monthly')"
start_timestamp="20180101"
end_timestamp="20211231"
output_filename="local_group_default_T_mediarequest_per_file.csv"

printf "SELECT * FROM \"local_group_default_T_mediarequest_per_file\".data 
 WHERE \"_domain\" = '$domain' AND referer IN $referers AND file_path IN $file_paths
 AND granularity IN $granularities AND timestamp >= '$start_timestamp' AND timestamp <= '$end_timestamp';" \
| cqlsh \
| sed '$d' \
| sed '$d' \
| sed -e 's/"/""/g' \
| sed -e 's/\ | /","/g' -e 's/ //g' -e '/^(/d' -e '/^--/d' -e '1d' \
| sed -e 's/^/"/g' -e 's/$/"/g' \
| sed -e 's/null//g' \
| sed -e 's/\\\\/\\/g' \
> $output_filename