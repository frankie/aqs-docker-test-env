# AQS Docker Test Environment

This is a Dockerized, single-node Cassandra test environment. It includes tooling to create a schema and load a small sample set for AQS endpoints. 

## Developer Guide: Getting Started

Each AQS service serves data from a Cassandra production cluster. This testing environment is meant to emulate the Cassandra production datastore. 

Each Go service is run from its own repository:

- [Page Analytics](https://gerrit.wikimedia.org/g/generated-data-platform/aqs/page-analytics)
- [Device Analytics](https://gerrit.wikimedia.org/g/generated-data-platform/aqs/device-analytics)
- [Media Analytics](https://gerrit.wikimedia.org/g/generated-data-platform/aqs/media-analytics)
- [Geo Analytics](https://gerrit.wikimedia.org/g/generated-data-platform/aqs/geo-analytics)

Follow the guide in each microservice's individual repository to start it up. To serve local test data, run this Cassandra test environment.

### Build and Run for developing purposes

This is the way to create and run this test environment in the case you are working on developing any AQS service and you need to run some request or your integration tests. 

In this case you'll run a docker compose project with a cassandra database with enough data to test properly any cassandra-based service (some details are available below).

You will need:
- Make
- Docker Compose

```shell
make startup
```

Wait until you see the 'Startup complete' log message, then in another terminal, bootstrap the database schema and sample data (keep in mind that, with the amount of data we have, this process takes around 15 minutes):

```shell
make bootstrap
```

### Build and Run for QA purposes

This is the way to create and run this test environment in the case you want to run the AQS QA test suite against a cassandra-based service.

In this case you'll run a docker compose project composed of:
- A cassandra database with enough data to test properly any cassandra-based service (some details are available below)
- Cassandra-based services according the docker-compose-qa.yml config file (some details are available below)

You will need:
- Make
- Docker Compose
- device-analytics, geo-analytics, media-analytics and page-analytics docker images already built (running ```make docker-qa``` from the service project)

```shell
make startup-qa
```

Wait until you see the 'Startup complete' log message, then bootstrap the database schema and sample data (keep in mind that, with the amount of data we have, this process takes around 15 minutes):

```shell
make bootstrap-qa
```

At the end of this process you will see a docker compose project, called _aqs-docker-test-env-qa_, running the following containers:
- __device-analytics-qa__: A container with the _device-analytics_ service listening on your local port 8090
- __media-analytics-qa__: A container with the _media-analytics_ service listening on your local port 8091
- __geo-analytics-qa__: A container with the _geo-analytics_ service listening on your local port 8092
- __page-analytics-qa__: A container with the _page-analytics_ service running on your local port 8093
- __cassandra-qa__: A cassandra container listening on the port 9042

### Troubleshooting

Other applications, notably the mwcli mediawiki local development tool, may also use this port. If the port is unavailable, shut it down in pageviews before starting this container:

In /pageviews:

```shell
make -C docker shutdown
```

## Sample data

Each dataset includes the most appropriate data for its respective endpoint. These specific details will be explained below:

### Page Analytics

Pageviews per article (local_group_default_T_pageviews_per_article_flat) counts the number of pageviews for a given article. This dataset includes the following information:

- Projects: "en.wikipedia"
- Articles: "Banana", "Earth", "Moon", "Wikipedia" and "Museo del Prado"
- Granularities: "daily" and "monthly" (the last one is not directly included in the dataset because data must be aggregated programatically)
- Date range: 2018.01.01 - 2021.12.31 (both included)

Pageviews per project (local_group_default_T_pageviews_per_project_v2) counts the number of pageviews on a given project by access type and agent. This dataset includes the following information:

- Projects: "en.wikipedia" and "all-projects"
- Access sites: "all-access", "desktop", "mobile-app" and "mobile-web"
- Agents: "all-agents", "automated", "spider" and "user"
- Granularities: "monthly", "daily" and "hourly"
- Date range: 2018.01.01 - 2021.12.31 (both included)

Top pageviews per project (local_group_default_T_top_pageviews) get the most viewed articles for a project. This dataset includes the following information:

- Projects: "en.wikipedia"
- Access sites: "all-access", "desktop", "mobile-app" and "mobile-web"
- Date range: 2019.01.01 - 2020.12.31 (both included). Data is already aggregated in the dataset for the "all-days" filter using that value as a day

Top by country pageviews (local_group_default_T_top_bycountry) get pageviews by country and access method. This dataset includes the following information:

- Projects: "en.wikipedia"
- Access sites: "all-access", "desktop", "mobile-app" and "mobile-web"
- Date range: 2018.01.01 - 2021.12.31 (both included)

Top per country (local_group_default_T_top_percountry) get the most viewed articles for a country. This dataset includes the following information:

- Country code: "GB"
- Access sites: "all-access", "desktop", "mobile-app" and "mobile-web"
- Date range: 2018.01.01 - 2021.12.31 (both included)

Legacy pagecounts (local_group_default_T_lgc_pagecounts_per_project) get unique devices count per project. This dataset includes the following information:

- Projects: "en.wikipedia"
- Access sites: "all-sites", "desktop-site" and "mobile-site"
- Granularities: "monthly", "daily" and "hourly"
- Date range: 2014.01.01 - 2016.05.05 (both included)

### Device Analytics

Unique devices (local_group_default_T_unique_devices) counts the number of unique device hits on a given project by access site. This dataset includes the following information:

- Projects: "en.wikipedia", "lmo.wikipedia", "bh.wikipedia", "ta.wiktionary", "it.wikibooks", "commons.wikimedia", "all-wikipedia-projects"
- Access sites: "all-sites", "desktop-site" and "mobile-site"
- Granularities: "monthly" and "daily"
- Date range: 2018.01.01 - 2021.12.31 (both included)

### Media Analytics

Mediarequests per file (local_group_default_T_mediarequest_per_file) counts the number of requests for a given file. This dataset includes the following information:

- Referers: "en.wikipedia", "all-referers", "internal', "external", "search-engine", "unknown" and "none"
- Agents: "all-agents", "user" and "spider"
- File paths: 
  - "/wikipedia/commons/1/1c/Manhattan_Bridge_Construction_1909.jpg"
  - "/wikipedia/commons/6/60/The_Earth_4K_Extended_Edition.webm"
  - "/wikipedia/commons/b/bd/Titan_%28moon%29.ogg"
  - "/wikipedia/commons/7/7e/NPS_craters-of-the-moon-map.pdf"
- Granularities: "monthly" and "daily"
- Date range: 2018.01.01 - 2021.12.31 (both included)

Top mediarequests (local_group_default_T_mediarequests_top_files) counts the most-viewed media properties for a given day by referer and media type. This dataset includes the following information:

- Referers: "all-referers", "internal" and "external"
- Media types: "all-media-types", "image" and "audio"
- Date range: 2019.01.01 - 2020.12.31 (both included). Data is already aggregated in the dataset for the "all-days" filter using that value as a day

Mediarequests per referer (local_group_default_T_mediarequests_per_referer) counts the total number of mediarequests for a given day by referer. This dataset includes the following information:

- Referers: "all-referers", "internal", "external", "search-engine", "unknown", and "none"
- Media types: "all-media-types", "image", "audio", "video", "document" and "other" 
- Agents: "all-agents", "spider" and "user"
- Granularities: "monthly" and "daily"
- Date range: 2018.01.01 - 2021.12.31 (both included)

### Geo Analytics

Editor by country (local_group_default_T_editors_bycountry) get editor count by country. This dataset includes the following information:

- Projects: de.wikipedia, fr.wikipedia and en.wikipedia
- Activity Levels: 5..99-edits and 100..-edits
- Date range: 2018.01.01 - 2021.12.31 (both included)

## Updating the included data sets

### Running the included scripts

A developer might have reason to update the data included for testing. This repository includes scripts that a developer with the appropriate access can modify and run in order to pull data from the production cluster. 

In order to follow this guide, you will need access to the AQS host and to the Cassandra cluster. 

The included script pulls data for the unique devices endpoint and transforms it to comma-delimited data. Once you have attained access, you can modify and run included scripts on the Cassandra cluster in order to copy and transform production data.

1. Copy the desired script to the target AQS server. For example, to generate a new csv file for the unique devices dataset, replacing "user" with your username on the AQS host,

    ```shell
    user@local:~/location/of/aqs-docker-test-env$ scp /scripts/local_group_default_T_unique_devices.sh aqs1010.eqiad.wmnet:/home/user/
    ```

2. ssh in to the server-- In this guide, we use AQS1010.

    ```ssh aqs1010.eqiad.wmnet```

    create cqlshrc file in the .cassandra directory on your home directory on the server, 

    ```cd .cassandra```

    ```touch cqlshrc```

    ***IMPORTANT*** change permissions so that only you can read or write this file,

    ```chmod 400 cqlshrc```

    and open the file.

    ```vim cqlshrc```

3. Add your credentials and the desired Cassandra cluster to connect to, in this format.

    ```shell
    [authentication]
    username = YOUR USERNAME
    password = YOUR PASSWORD

    [connection]
    hostname = aqs1010-a.eqiad.wmnet
    ```

4. run the script in your home directory on the host.

    ```./local_group_default_T_unique_devices.sh```

    If all goes according to plan, you'll have a file named expected_filename.csv in your home directory on the target AQS server.

4. Copy this file to the desired location on your home machine. You can copy it directly into the /test_data.d folder in the test environment. Ensure that the name of the file follows the format dictated in the schema: local_group_default_T... For example,

    ```shell
    user@local:~/location/of/aqs-docker-test-env$ scp aqs1010.eqiad.wmnet:/home/user/local_group_default_T_unique_devices.csv test_data.d
    ```

5. From here, you should be able to startup and boot the test environment completely as expected (See: Build and Run, above).

6. Once you've successfully updated the necessary test data, remove the cqlshrc file from your home directory on the AQS server.