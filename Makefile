# Copyright 2021 Eric Evans <eevans@wikimedia.org> and Wikimedia Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

SERVICE = cassandra

# Developer targets: to create and environmet to develop
bootstrap: schema load

cqlsh:
	docker-compose run --rm "$(SERVICE)" cqlsh "$(SERVICE)"

schema:
	docker-compose run --rm "$(SERVICE)" cqlsh "$(SERVICE)" -f /env/schema.cql

load:
	docker-compose run --rm "$(SERVICE)" cqlsh "$(SERVICE)" -f /env/test_data.cql --cqlshrc=/env/cqlshrc

startup:
	docker-compose -f docker-compose.yml up -d

# QA targets: to create an environment for QA testing
bootstrap-qa: schema-qa load-qa

schema-qa:
	docker exec -it cassandra-qa cqlsh -f /env/schema.cql

load-qa:
	docker exec -it cassandra-qa cqlsh -f /env/test_data.cql --cqlshrc=/env/cqlshrc

startup-qa:
	docker-compose -f docker-compose-qa.yml up -d

# Generic targets
shutdown:
	docker-compose down --remove-orphans

.PHONY: bootstrap cqlsh load schema startup bootstrap-qa load-qa schema-qa startup-qa shutdown 
